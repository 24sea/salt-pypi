# salt-pypi

## Pillar

Control which minions are able to access the private pip index.

Essentially all need to have access to install the `prtg` package enabling the _prtg_ returner.
