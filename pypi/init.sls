{% set pypi_port = salt['pillar.get']('pypi:port_internal', '8080') %}
{% set pypi_interface = salt['pillar.get']('pypi:interface', '127.0.0.1') %}
{% set package_directory = salt['pillar.get']('pypi:package_directory', '/var/pypi') %}
{% set htpasswd_file = salt['pillar.get']('pypi:htpasswd_file', '/root/htpasswd.txt') %}
{% set domain_name = salt['pillar.get']('pypi:server') %}
{% set disallow_countries = salt['pillar.get']('pypi:disallow_countries') %}
{% set pypi_ssl = salt['pillar.get']('pypi:ssl', True) %}

{% if not salt['pillar.get']('testing', False) %}
include:
  - linux.nginx
{% endif %}

pypiserver_installs_via_pip:
  pkg.installed:
    - name: python3-pip

{{ package_directory }}:
  file.directory

{{ htpasswd_file }}:
  file.managed:
    - replace: False

{% for entry in pillar['htpasswd'] %}
{% for key, value in entry.items() %}
accumulate_{{ key }}:
  file.accumulated:
    - filename: {{ htpasswd_file }}
    - name: accumulate_{{ key }}
    - text: {{ key }}:{{ value }}
    - require_in:
      - file: htpasswd_file
{% endfor %}
{% endfor %}

htpasswd_file:
  file.blockreplace:
    - name: {{ htpasswd_file }}
    - marker_start: "# START managed zone -DO-NOT-EDIT-"
    - marker_end: "# END managed zone --"
    - append_if_not_found: True

{% for pkg in ["pypiserver", "passlib"] %}
pypiserver pip package {{ pkg }}:
  pip.installed:
    - name: {{ pkg }}
    - bin_env: /usr/bin/pip3
    - require:
        - pkg: python3-pip
{% endfor %}

pypiserver:
  file.managed:
    - name: /etc/systemd/system/pypiserver.service
    - source: salt://pypi/pypiserver.service
    - template: jinja
    - context:
        htpasswd_file: {{ htpasswd_file }}
        package_directory: {{ package_directory }}
        port: {{ pypi_port }}
        interface: {{ pypi_interface }}

  service.running:
    - enable: True
    - pkg: pypiserver
    - watch:
      - file: /etc/systemd/system/pypiserver.service
      - file: {{ htpasswd_file }}

# authentication
passlib:
  pip.installed

# syslog-ng
syslog_conf:
  file.managed:
    - name: /etc/syslog-ng/conf.d/pypi.conf
    - source: salt://pypi/syslog_pypi.conf
    - onlyif: test -d /etc/syslog-ng

  service.running:
    - name: syslog-ng
    - watch:
      - file: syslog_conf

# add nginx reverse proxy
nginx_pypi:
  file.managed:
    - name: /etc/nginx/sites-available/pypi.conf
    - source: salt://pypi/nginx_pypi.conf
    - template: jinja
    - context:
        port: {{ pypi_port }}
        interface: {{ pypi_interface }}
        domain_name: {{ domain_name }}
        disallow_countries: {{ disallow_countries }}
        ssl: {{ pypi_ssl }}

nginx_pypi_enable:
  cmd.run:
    - name: ln -s /etc/nginx/sites-available/pypi.conf /etc/nginx/sites-enabled/pypi.conf
    - creates: /etc/nginx/sites-enabled/pypi.conf

# let's encrypt
renew_cert:
  cron.present:
    - name: /usr/bin/certbot renew --quiet
    - user: root
    - minute: 0
    - hour: 5
