control 'pypiserver' do
  impact 0.9
  title 'pypiserver installation and service configuration'
  desc 'Package installed through pip and systemd service created'
  tag ''

  describe pip('pypiserver') do
    it { should be_installed }
  end

  describe pip('passlib') do
    it { should be_installed }
  end

  describe ini('/etc/systemd/system/pypiserver.service') do
    its('Service.Type') { should cmp "Simple" }
    its('Service.ExecStart') { should match /pypi-server run/ }
  end

  describe systemd_service('pypiserver') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end
