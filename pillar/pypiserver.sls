#!yaml|gpg
pypi:
  server: pypi.24sea.eu
  port: 443
  username: salt
  # URL encoded
  password: |
    -----BEGIN PGP MESSAGE-----

    hQGMA3zud4ES7+OcAQv/Vr/6pl6blRSGatMY7fpqeKVz9mGOZkRARlxE9m0aMa4z
    8e41P+/zhimbLfSPilH95gfh3vd8Xpqx1MRwJm9yckJ5tOQmWv+bjkE+/VgYFUSj
    dsQvRI2Q9n2ODEEbkHHMHy155+1DpQfS29P9zDi3/xHvGklD2VOeIAA7P5snDDIv
    6VvcIZeNWrEp6j9dwxNsC7WCkWtV6ARJTo4qZuh6gI+TDjldvR9OK0z0aK2njr5V
    Gf+4oP7aNlmOz28Zw6QP5UTF2richWqK5h3UL7SRBLT3uYyPO4slP0NSnGi17Yr+
    PH1gGyXsyS0owSDFGcrOEfLCtdsgg/Tfhh8B70cx0sWd4YepnZsboznaGrVPnkFP
    SHZnufdx5zgxWJ0bTVHobeU8nbj2cVZGAKZX3RddXvMQpiEC2gf5DDD+/I+1I8U+
    PWt9zDZ3nYKdQl2OMXuvj6WarYoTu/AlZBpVshP5LqxpJS4VBYxZb0YXcfBRnFlD
    PfKYJY99dM4noaoi1V/X0mQBDGaHBbdEScMBEEFCRvwV8RopkwQa3q3JIaZYMpaT
    sYdRvZJZAvbsztBQclr2Ke6ipPQOh8zE9jeBeFeoXz4QjyqCD2vCagbfiMd6cBDO
    GW0tTRwr/fi5beBJyivfJB+2WsZb
    =kzJB
    -----END PGP MESSAGE-----
  ssl: True
