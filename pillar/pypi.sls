#!yaml|gpg
# needs to be SHA1 (insecure) on this generator https://hostingcanada.org/htpasswd-generator/
htpasswd:
  - salt: |
      -----BEGIN PGP MESSAGE-----

      hQGMA3zud4ES7+OcAQv/cJiOyaRqVTB061wkN2YESMf8hCoxPAwvXg71yMQc8aXR
      /zkbPs5CKsTbluZf+mZ6H+HcmI2T5HNEqyatKD+4yrmBlCaVQ0/osLNAidSY6nSh
      OwzA5HymUrdljDMoa9XQ7dxt1cS1IyQZqhG3Kw5tz9wvjZv2CHdsJY1dANuL57zx
      vPkEh6KdZGI2bY/bRBtJBPczDTul/KPTJKP5//6Ahk4I6Cpks+UdGHaUGjDY1pW3
      4bW0pHqe5CFuE8za+DTJIcsEUQnBvZnWXwJW8TXuZRvatwc0y//TNBTbIGWYpu1x
      UPsv07/IcHlu+iEWXt+dVJngbNwSClMlozb6J/UAaL4kBwUuy162RRERKMw1pytl
      sUwezfnwZR9YJnJs3L9E/6eqFB2WVYSvVlM1zFNTkSlWNfKHfyTQGD688vN8Za3B
      c3KKUXkSi90tHE+tQl1UttKYZyHDsu/Jt6janTdHezdtfREAYlamBjuhIK68jDHq
      dlk4W0Cy3SZuA2fP1sKJ0lwBluIX57w8MGya05qXJbCb0E0CiuWWgzzQ/QmRw/Bb
      3uMPdZORVCwbxueBcJfqPx1V6NwVwaz2yGazTo3jTxRyeg5jL1FhkSJainLDVSK2
      RQVyPcBzTklKAS5ogg==
      =EWcW
      -----END PGP MESSAGE-----
  - pipelines: |
      -----BEGIN PGP MESSAGE-----

      hQGMA3zud4ES7+OcAQwAqtaPK2gzl0xJkMaXXsVnwZBCeIHat5fVyJcGp80d5A44
      FZprrYkz8A2C3HD1bZJ+eGyJwjzlLF+WCxA5TguDNVAXCryQ4CLya6OB5sNv+LXz
      B8ZNF44kiQJj8VHiJVb/vlWHkbYFydkK9YmsnvqXJIDaOOAnjDccwweFclyXR/cv
      VsB2b4NGXkCPrlOgkayTjOUoBHdLns/mdCrEGa8AoHcQn1lCJJ4xbZSPlW5F8qf3
      0nW9UvfOiGz6fiBxvHKmiawxnsNhSmRkF9E3gZS0ZkaHxUL9cwAuGGPliT/2bz/0
      J0hwmKr5ul+ug6pLtuBZkGwl0n/c/W5t4mdmeJ137mv0rF6H+UPulBx9ZQTP8WFe
      HOWEwSFIiwI9vzEE0NCdC4larJA5y9gWuhnH7MytizxdN30NX7XzTEcUUOKgxNuo
      hFqHOZfP+EgcgLH4nSXEtDQOpylDanQ6VOdVJFvl/kq+nv3BlbdSP7FJQqTmXmMS
      mV2H1KqjcQZqeshzF0aT0lwBwty62T71swh0n43+0RVTN5fCAf4jv0h7QQkbBOQq
      9UFKtMx67j9OYy5jJaDhj2GndmKGlbCnaKFRhlGU+yNhTPKmRdUuH6/M0ZWogCfy
      U3WFEqQnorpav30VYg==
      =9HEH
      -----END PGP MESSAGE-----
  - tim: |
      -----BEGIN PGP MESSAGE-----

      hQGMA3zud4ES7+OcAQv+LwHp84jGgK6misbtsD5QN8JIqjX1raTE+ow6rzHfNq3w
      PrqZaxT+WwY5jNUJP3rlXNfY4zhECNteKjRgMwPxmt9EJriF/2B59m501H2jwWV3
      32LSskIXaeQUX41zFaD5+HYNdoIB6GN2Hb4TrJrx1L9Un/1jVZMy7m8hqo5C79BT
      AP0OOsHPRhQWxbGocjEqmMcsrhphSPgBG+zOzTSHSHGlCuSIkaWX23TRzXFpgT8t
      JOy5fwafB29SvScpdMralHhlRlol0kfDXMuFtkG7hZujJA7lp/ViGM+tCmZNg0Um
      COYxTiN9kY7LbZyRBfG5+s+sqcUoC5RAJFxvZEun5xtqVgSnIEa1wZRwNTXgbOmq
      OQGVIBWBy+PvEyd+Og5kwaD4xus+37E7kNgx1UJW8NfJiwfb5XDmhcFiPB1jaokv
      S1Chl663gQH1CS1GyT7+msrPN1PJ9D0JDp1T1k1bvzI7hgGoSGAt1pqn0CAecE1g
      5EKDQWRUQiA6EvzqGLUJ0lwByJYkbB5hRm2FFrGUskRb+9Si5ZwY8fOiayuF7ovp
      q/7I7sPb0J0Fj4vlTKQdUrtyuHy7y2lUuvq2zqe+yXcM1b2TEiy75yM/0NWDg3j0
      Pk3zxNrQy7rt+XhuNw==
      =ZaAc
      -----END PGP MESSAGE-----
  - wim: |
      -----BEGIN PGP MESSAGE-----

      hQGMA3zud4ES7+OcAQv8Dgo8McLAKHhly+322dC3/F+aDJWcyOYa2ZCnt3u71u2y
      nhc8QrA7ALHpTM5FUClwTu3aNonpel05SvdOc4Xlv8d4uJ059jrGPGlZvE0m4tMd
      YBaJw9qBHpA3IMLYD2ddAyKErv2Tzm6kNutGOV+8O/ioykkYp1PjLCz+vG1GAoc3
      ZvtXdSIn6cknJDDXJLulZnLb9o9UtKrpZG0pJxaFz+NF7lNRBrwi+wzFpvahP0qB
      QgLQ9wHLLfwxDtiGn3LFWJzMiDiicy7HXJyVxZMVLaPHbysJnwn/Q5DDYFeQfxlt
      631XQZ4M0fDgHYFsgBaTOKPI9+L3tC0bA1XrldJ2RVl4F9THUfhrEu+iG3DvGO8a
      WhaczZtCVtBN/pvrYzma8GFwmXxT+fUSwFWepLyea7+Uyk/d/s9IYDGoYwVdC2QH
      p/fa0QALXTBC0naF7+jtnvLQopNklSeYJNs00jNNWVFgdehZmMcumQRxLCAHtG/1
      2u+6ifDn3BP6pnjarH0R0lwBYZxA65J93b3slWL0r5C3p8AA8XVW4R5sHtHR1G3i
      lKmHLDgmIj4oyw208TdUms5WX2Sz+wdJajvPPr2wmQf1uab5DRL+sTDwuHUuUl27
      0p0IT8gjEURsdLG3yw==
      =uA6a
      -----END PGP MESSAGE-----
  - pieter: |
      -----BEGIN PGP MESSAGE-----

      hQGMA3zud4ES7+OcAQv+JpLHw2RMnnh+JRtrz79aqSYpd9sqScEpuaT7A0Oy9LJe
      lTqt5HQImdbKBqNfP2sGGRDarC/yoAeQDTwyXvRkkAk4bwYttCHyNs5ag13LR9hi
      Ke1sKmC6amhcK9uUu60tvfJ7T5CRo/hT3F3/kIkCGebHTgbRQYgRPBalPy0RUiIr
      BeY/z0/IWdiF5OnI3N7Tj2W1MHSCQ12wqQrjAhIpmSnk+YrzyqifL1U1CLQY3hft
      uY+NotRrBjfemQ8zCZg82sMCvSDJY6bl+UfcOkyJyZEyTsjvvJTP5UVMECehYJHN
      Af85qQEypTa3bI9+Xmzszrvb36jRlbgaQ26VacWTc8oEwsZ0APB9zQ6BXlGBk1h1
      U4bKRigXjSYL45pxJjWazbVw2pj1I/oz3ynhTYKvftbCyWzJFtZBv0XwXXKGgsiX
      PidMnHVJPV7hKCtFqFF9AIgWv8TqzUUsvqK9jsf7NI1kO2xMd+FKl9AZUI5PMC4i
      Ea4o/s6U6jGJXMFx3bXk0mEBYAQYsyJuNtSPqHKOgbLQwvhyy2UWAJ7pKcjFRtYB
      1nadseHK+FTkcqR05cfm62QrsdDxfQHEkRvxRkwzVFSNCtMTXY4QTbzTJQZrN5tg
      DUAvC9B53xeEuXqftL1JfGeI
      =KG2+
      -----END PGP MESSAGE-----
  - michiel: |
      -----BEGIN PGP MESSAGE-----

      hQGMA3zud4ES7+OcAQv/fYZLBQLMAVuJb8nZfs8sxlnr+cWSousCDq176c3nSUZA
      Q6bubI+X77onbepzBMoghkm9dqer5ZsLAhBwkCEKA+I80pN6OIAR4JreSTFKT9OY
      MytYFBArvo11n6snCgSyXqaCO2sCMlIa2VDfypG3/GYcBPTUvM77SLeVbewYNmhH
      ZCpN4h40zW/Zwm5gY4Xg9kfTqAxv4+7ACM7NkCTjMUraMz8pHCxwNCnJK4v7Zvqp
      MK+zilslbpEH7XcstOOpqeoPDhleuECskfRTWmGRZa/2qxlVbIlcFhPXzlPm2ard
      5iq/22HOoqZijKJ9iqRIuF0KL0uaagXYpIGiBhbNGz3yseno8wPfnNXNRNam4q4A
      9z1nOgdHHTIM+4qpiyF/YNlG0LH0mAkT4J4oWgxhU+SgUskTa9ZAzYxHbYV7O+LD
      vqsA/yGMsXmNq/G8u/ji9/iMt7ByNmg3+fd9C9gEkJeofX429GA4almSiV4T3njV
      qRLExVuMXeMRfGp7awUf0mEBo5Q32w9uoTMYKMSvwwKDkYfK3fgoEB+2ZzzckF4M
      zi29TRQYYxFNwSv0FSTVng4ON3BYB4kI0MKrJZdPpHNoSrMKvR7Gjb/SA443rDAJ
      wEfLvlMHbmFjyg6kmuBRM1lm
      =y0Y7
      -----END PGP MESSAGE-----

prtg:
  ip: prtg.24sea.eu
  guid: a97e14f8-2d1b-4def-be55-2612570cdf86

pypi:
  verbosity: v
  log_file: /var/log/pypiserver.log
  allow_countries:
    - BE
    - DE
    - NL
    - UK
    - FR
  allow_ranges:
    - 18.34.0.0/19
    - 16.15.192.0/18
    - 54.231.0.0/16
    - 52.216.0.0/15
    - 3.5.76.0/22
    - 18.34.244.0/22
    - 18.34.48.0/20
    - 18.34.232.0/21
    - 3.5.80.0/21
    - 16.15.176.0/20
    - 52.218.128.0/17
    - 16.182.0.0/16
    - 3.5.0.0/19
    - 52.92.128.0/17
    - 44.192.134.240/28
    - 44.192.140.64/28
    - 35.80.36.208/28
    - 35.80.36.224/28
    - 15.181.232.0/21
    - 3.2.0.0/24
    - 18.97.0.0/18
    - 52.4.0.0/14
    - 96.0.102.0/23
    - 5.60.40.0/22
    - 5.60.200.0/22
    - 50.16.0.0/15
    - 139.56.16.0/23
    - 15.220.196.0/22
    - 161.188.116.0/22
    - 64.252.69.0/24
    - 35.96.240.0/24
    - 3.4.0.0/24
    - 15.220.252.0/22
    - 54.148.0.0/15
    - 99.77.130.0/24
    - 99.150.56.0/21
    - 15.220.222.0/23
    - 23.228.193.0/24
    - 15.181.247.0/24
    - 18.232.0.0/14
    - 155.146.80.0/20
    - 15.220.120.0/21
    - 15.220.207.0/24
    - 155.146.224.0/20
    - 15.193.7.0/24
    - 54.156.0.0/14
    - 54.236.0.0/15
    - 99.150.8.0/21
    - 155.146.240.0/20
    - 15.181.160.0/20
    - 15.181.80.0/20
    - 5.60.72.0/22
    - 136.18.128.0/23
    - 107.20.0.0/14
    - 155.146.176.0/20
    - 54.25.15.0/24
    - 35.96.11.0/24
    - 18.236.0.0/15
    - 54.200.0.0/15
    - 54.144.0.0/14
    - 5.60.144.0/22
    - 35.50.133.0/24
    - 64.252.72.0/24
    - 139.56.32.0/23
    - 5.60.188.0/22
    - 75.101.128.0/17
    - 96.0.16.0/21
    - 3.4.3.0/24
    - 155.146.192.0/20
    - 5.60.156.0/22
    - 52.94.249.64/28
    - 67.202.0.0/18
    - 5.60.104.0/22
    - 15.181.253.0/24
    - 18.34.0.0/19
    - 54.226.0.0/15
    - 161.188.120.0/22
    - 162.250.237.0/24
    - 64.187.128.0/20
    - 64.252.125.0/24
    - 3.224.0.0/12
    - 54.221.0.0/16
    - 35.50.132.0/24
    - 15.181.112.0/22
    - 68.66.112.0/20
    - 70.224.192.0/18
    - 15.181.241.0/24
    - 34.192.0.0/12
    - 13.216.0.0/13
    - 15.220.32.0/21
    - 35.96.1.0/24
    - 64.252.68.0/24
    - 5.60.48.0/22
    - 99.77.191.0/24
    - 44.192.0.0/11
    - 3.4.9.0/24
    - 162.250.238.0/23
    - 54.245.0.0/16
    - 16.15.192.0/18
    - 136.18.138.0/23
    - 99.77.152.0/24
    - 35.96.7.0/24
    - 155.146.128.0/20
    - 23.20.0.0/14
    - 64.252.64.0/24
    - 3.4.1.0/24
    - 15.220.184.0/21
    - 5.60.128.0/22
    - 35.160.0.0/13
    - 5.60.64.0/22
    - 15.181.144.0/20
    - 5.60.80.0/22
    - 15.181.254.0/24
    - 54.112.0.0/18
    - 155.146.0.0/20
    - 155.146.112.0/20
    - 99.151.190.0/23
    - 5.60.56.0/22
    - 18.98.0.0/18
    - 54.68.0.0/14
    - 52.44.0.0/15
    - 54.212.0.0/15
    - 15.181.176.0/20
    - 15.220.202.0/23
    - 35.50.129.0/24
    - 15.181.48.0/20
    - 96.0.131.0/24
    - 192.157.36.0/24
    - 15.193.6.0/24
    - 52.90.0.0/15
    - 139.56.20.0/23
    - 15.220.176.0/21
    - 52.0.0.0/15
    - 208.110.48.0/20
    - 151.148.36.0/24
    - 52.54.0.0/15
    - 52.95.230.0/24
    - 99.77.253.0/24
    - 3.4.6.0/24
    - 15.220.208.128/26
    - 35.80.0.0/12
    - 3.4.10.0/24
    - 99.77.129.0/24
    - 15.220.200.0/23
    - 18.96.0.0/24
    - 139.56.24.0/23
    - 5.60.88.0/22
    - 151.148.33.0/24
    - 35.50.134.0/24
    - 35.153.0.0/16
    - 161.188.127.0/24
    - 52.12.0.0/15
    - 162.250.236.0/24
    - 18.96.1.0/24
    - 52.200.0.0/13
    - 34.224.0.0/12
    - 52.75.0.0/16
    - 96.0.88.0/22
    - 136.18.50.0/23
    - 13.130.0.0/16
    - 54.218.0.0/16
    - 3.5.76.0/22
    - 15.181.0.0/20
    - 54.25.14.0/24
    - 139.56.28.0/23
    - 35.96.4.0/24
    - 139.56.26.0/23
    - 155.146.160.0/20
    - 54.244.0.0/16
    - 96.0.100.0/23
    - 161.188.16.0/20
    - 63.246.119.0/24
    - 155.146.208.0/20
    - 35.96.6.0/24
    - 50.19.0.0/16
    - 5.60.120.0/22
    - 5.60.172.0/22
    - 44.224.0.0/11
    - 136.18.130.0/23
    - 3.2.3.0/24
    - 206.72.209.0/24
    - 64.252.73.0/24
    - 161.188.80.0/20
    - 174.129.0.0/16
    - 18.89.128.0/18
    - 52.95.255.80/28
    - 35.50.130.0/24
    - 52.95.255.112/28
    - 96.0.12.0/22
    - 15.181.224.0/21
    - 18.208.0.0/13
    - 35.96.16.0/20
    - 52.95.245.0/24
    - 99.77.187.0/24
    - 136.18.132.0/23
    - 35.96.8.0/24
    - 100.20.0.0/14
    - 98.88.0.0/13
    - 5.60.136.0/22
    - 15.220.0.0/20
    - 75.2.128.0/18
    - 184.72.128.0/17
    - 15.220.16.0/20
    - 54.80.0.0/13
    - 54.214.0.0/16
    - 52.20.0.0/14
    - 52.94.201.0/26
    - 139.56.18.0/23
    - 54.242.0.0/15
    - 139.56.34.0/24
    - 216.182.238.0/23
    - 34.208.0.0/12
    - 98.80.0.0/13
    - 23.228.192.0/24
    - 35.71.64.0/22
    - 18.88.0.0/18
    - 18.34.244.0/22
    - 208.86.88.0/23
    - 54.208.0.0/15
    - 18.96.2.0/24
    - 52.36.0.0/14
    - 161.178.128.0/18
    - 15.220.226.0/24
    - 35.50.128.0/24
    - 16.56.128.0/18
    - 99.77.128.0/24
    - 161.188.0.0/20
    - 54.152.0.0/16
    - 35.50.135.0/24
    - 54.202.0.0/15
    - 15.181.128.0/20
    - 52.70.0.0/15
    - 52.94.248.0/28
    - 96.0.56.0/22
    - 5.174.0.0/16
    - 15.220.204.0/24
    - 96.0.84.0/22
    - 99.77.254.0/24
    - 70.232.92.0/22
    - 96.0.110.0/23
    - 15.181.245.0/24
    - 35.71.68.0/22
    - 52.95.247.0/24
    - 15.177.64.0/23
    - 46.51.208.0/22
    - 52.86.0.0/15
    - 54.25.20.0/24
    - 50.112.0.0/16
    - 15.220.233.0/24
    - 15.181.64.0/20
    - 54.172.0.0/15
    - 18.204.0.0/14
    - 54.88.0.0/14
    - 52.94.116.0/22
    - 173.83.200.0/22
    - 15.181.248.0/24
    - 3.2.2.0/24
    - 64.252.66.0/24
    - 3.3.5.0/24
    - 52.2.0.0/15
    - 155.146.64.0/20
    - 184.72.64.0/18
    - 15.181.192.0/19
    - 15.253.0.0/16
    - 216.182.232.0/22
    - 15.181.252.0/24
    - 52.46.180.0/22
    - 161.188.32.0/20
    - 16.56.0.0/18
    - 99.151.184.0/23
    - 155.146.144.0/20
    - 15.181.242.0/24
    - 54.25.82.0/24
    - 204.236.192.0/18
    - 35.168.0.0/13
    - 15.181.40.0/21
    - 18.34.48.0/20
    - 18.34.232.0/21
    - 72.44.32.0/19
    - 15.181.16.0/20
    - 15.181.96.0/20
    - 13.128.0.0/16
    - 35.96.244.0/24
    - 162.222.148.0/22
    - 3.4.2.0/24
    - 52.24.0.0/14
    - 64.252.65.0/24
    - 54.92.128.0/17
    - 23.228.194.0/24
    - 99.77.151.0/24
    - 216.182.224.0/21
    - 5.60.180.0/22
    - 136.18.254.0/23
    - 18.246.0.0/16
    - 54.204.0.0/15
    - 54.196.0.0/15
    - 3.5.80.0/21
    - 54.26.166.0/24
    - 155.146.32.0/20
    - 15.220.220.0/23
    - 52.72.0.0/15
    - 16.15.176.0/20
    - 54.160.0.0/13
    - 54.234.0.0/15
    - 5.60.164.0/22
    - 15.220.248.0/23
    - 136.18.136.0/23
    - 15.220.250.0/23
    - 15.220.236.0/22
    - 35.96.2.0/24
    - 155.146.48.0/20
    - 161.188.48.0/20
    - 15.181.249.0/24
    - 15.220.205.0/24
    - 52.88.0.0/15
    - 139.56.22.0/23
    - 35.96.10.0/24
    - 99.77.232.0/24
    - 161.193.0.0/18
    - 142.4.178.0/24
    - 5.60.196.0/22
    - 3.208.0.0/12
    - 52.129.224.0/22
    - 161.178.0.0/18
    - 15.220.206.0/24
    - 184.73.0.0/16
    - 15.177.80.0/24
    - 35.96.241.0/24
    - 54.174.0.0/15
    - 155.146.16.0/20
    - 54.224.0.0/15
    - 96.0.160.0/20
    - 15.220.234.0/23
    - 99.151.186.0/23
    - 15.254.0.0/16
    - 35.96.144.0/20
    - 96.0.96.0/22
    - 3.80.0.0/12
    - 52.40.0.0/14
    - 155.146.96.0/20
    - 54.239.103.128/26
    - 64.252.70.0/24
    - 96.0.104.0/22
    - 18.88.128.0/18
    - 35.96.15.0/24
    - 204.87.185.0/24
    - 5.60.112.0/22
    - 52.32.0.0/14
    - 5.60.96.0/22
    - 64.252.67.0/24
    - 54.198.0.0/16
    - 35.96.14.0/24
    - 35.96.245.0/24
    - 15.181.244.0/24
    - 54.184.0.0/13
    - 15.181.120.0/21
    - 173.83.204.0/23
    - 15.181.251.0/24
    - 3.5.0.0/19
    - 64.252.71.0/24
    - 15.181.246.0/24
    - 72.41.0.0/20
    - 35.155.0.0/16
    - 54.210.0.0/15
    - 15.181.240.0/24
    - 192.43.175.0/24
    - 52.10.0.0/15
    - 100.24.0.0/13
    - 139.56.30.0/23
    - 3.4.4.0/24
    - 3.3.32.0/23
    - 96.0.152.0/21
    - 161.188.112.0/22
    - 15.181.32.0/21
    - 15.181.116.0/22
    - 15.181.243.0/24
    - 35.96.3.0/24
    - 96.0.92.0/23
    - 15.220.224.0/23
    - 161.188.64.0/20
    - 3.33.34.0/24
    - 15.181.250.0/24
    - 35.50.131.0/24
    - 35.96.0.0/24
    - 35.96.9.0/24
    - 3.3.2.0/24
    - 15.220.40.0/22
    - 52.94.248.96/28
    - 96.0.48.0/21
    - 99.77.186.0/24
    - 35.96.13.0/24
  port_internal: 8080
